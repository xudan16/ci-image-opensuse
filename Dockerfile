FROM opensuse/leap:15.4

LABEL \
	maintainer="Sylva team" \
	repo="https://gitlab.com/xudan16/ci-image-opensuse.git"

ARG TARGETOS
ARG TARGETARCH
ARG YQ_VERSION

RUN zypper install -y docker wget gettext
RUN wget -q --show-progress --progress=bar:force https://github.com/mikefarah/yq/releases/download/v${YQ_VERSION}/yq_${TARGETOS}_${TARGETARCH} -O /usr/local/bin/yq
RUN chmod +x /usr/local/bin/yq
